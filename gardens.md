Garden Designs
--------------

rubber tubing   e = 0.025  mm, D=1 in.
PVC             e = 0.0015 mm, D=2.5 in.
(where is this from? need to cite)

component order (two variations):

water pressure:
  - municipal supply: 10,15,20 ft H₂O
  - tinaco supply: 5-10 ft H₂O

1)  - "street value" pressure head  (based on #2)
  - 20m from municipal supply to garden, PVC
  - PVC lateral

2)  - *tinaco* pressure head  (based on #2)
  - gravity supply
  - 3m from tinaco to garden, PVC

3)  - "street value" pressure head  (based on #5)
  - supply from street, ABS tubing
  - ABS lateral

Listing of edges & nodes
------------------------
1)
  - n1: [10, 15, 20] * un.ftH2O
  - e1: 20m PVC
  - n2: 90° bend
  - e2: 2m PVC
  - n3: 90° bend
  - e3: 1m PVC
  - n4: P1
  - e4: 1m PVC
  - n5: P2
  - e5: 1m PVC
  - n6: P3
  - e6: 1m PVC
  - n7: P4
  - e7: 1m PVC
  - n8: P5
  - e8: 1m PVC
  - n9: P6

2)
  - n1: 5 * un.ftH2O
  - e1: 20m PVC
  - n2: 90° bend
  - e2: 2m PVC
  - n3: 90° bend
  - e3: 1m PVC
  - n4: P1
  - e4: 1m PVC
  - n5: P2
  - e5: 1m PVC
  - n6: P3
  - e6: 1m PVC
  - n7: P4
  - e7: 1m PVC
  - n8: P5
  - e8: 1m PVC
  - n9: P6

3)
  - n1: [10, 15, 20] * un.ftH2O
  - e1: 20m ABS
  - n2: 90° bend
  - e2: 2m ABS
  - n3: 90° bend
  - e3: 1m ABS
  - n4: P1
  - e4: 1m ABS
  - n5: P2
  - e5: 1m ABS
  - n6: P3
  - e6: 1m ABS
  - n7: P4
  - e7: 1m ABS
  - n8: P5
  - e8: 1m ABS
  - n9: P6
