import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from iapws import IAPWS97 as water
import fluids as f
from pint import UnitRegistry

np.set_printoptions(threshold=3, linewidth=200)

un = UnitRegistry()
w = water(T=273.15+25, P=1.01325)
WATER_DENSITY = w.rho*un.kg/un.m**3
WATER_KINEMATIC_VISCOSITY = w.nu
pi = np.pi

ABS = {
    'Do': (1*un.inch).to('m').magnitude, # outer diameter estimate
    'e': 0.025 # mm basis
}
PVC = {
    'Do': (2.5*un.inch).to('m').magnitude, # outer diameter estimate
    'e': 0.0015 # mm basis
}
for pipe in (ABS, PVC):
    # Find nearest standard pipe and use inner diameter
    pipe['Di'] = f.piping.nearest_pipe(Do=pipe['Do'])[1]


supply_pressure_list = [10, 20]*un.ftH2O

"""
drip tape
at 10 psi, 40gph
"""
drip_flowrates = [
    (25*un.gallon/un.hour).to('m**3/s'), # at 4 psi, 28 kPa
    (40*un.gallon/un.hour).to('m**3/s') # at 8 psi, 55 kPa
]
V = [Q*6/(pi/4 * (PVC['Di']*un.m)**2) for Q in drip_flowrates]
tape_area = (pi/4 * (5/8*un.inch)**2).to('m**2')
velocities = [Q/tape_area for Q in drip_flowrates]
supply_pressure_list = [P.to('Pa') for P in supply_pressure_list]
abs_eD = f.core.relative_roughness(ABS['Di'], roughness=ABS['e'])
pvc_eD = f.core.relative_roughness(PVC['Di'], roughness=PVC['e'])

reynolds_numbers = [f.core.Reynolds(V=v, D=ABS['Di'], nu=WATER_KINEMATIC_VISCOSITY).magnitude for v in velocities]
friction_factors = [f.friction.friction_factor(Re=r, eD=abs_eD) for r in reynolds_numbers]
bends = [f.fittings.bend_rounded(Di=PVC['Di'], rc=PVC['Di'], angle=90.0, fd = fric) for fric in friction_factors]
# major loss from street to garden
friction_losses = [f.core.K_from_f(f=fric, L=12, D=ABS['Di']) for fric in friction_factors]
# add up all minor loss coefficients
# ball valve: 0.05, unions: 0.08 (times 8)
netK = [0.05 + 0.08 * 8 + bend + friction_losses[key] for key, bend in enumerate(bends)]

# use avg velocity between street and row 1 to find pressure drop
dPs = []
for key, v in enumerate(velocities):
    dPs.append(f.core.dP_from_K(K=netK[key], rho=WATER_DENSITY, V=v).magnitude * un.Pa)
# then repeat for all 6 pressure heads (need in psi)
# also need flow rate (in gpm)
# total pressure loss to first spot
# now back-calculate avg velocity
v_avg = [(velocities[key] + V[key])/2 for key, v in enumerate(V)]
reynolds_numbers = [f.core.Reynolds(V=v, D=ABS['Di'], nu=WATER_KINEMATIC_VISCOSITY).magnitude for v in v_avg]
friction_factors = [f.friction.friction_factor(Re=r, eD=abs_eD) for r in reynolds_numbers]
bends = [f.fittings.bend_rounded(Di=PVC['Di'], rc=PVC['Di'], angle=90.0, fd = fric) for fric in friction_factors]
# major loss from street to garden
friction_losses = [f.core.K_from_f(f=fric, L=12, D=ABS['Di']) for fric in friction_factors]
# add up all minor loss coefficients
# ball valve: 0.05, unions: 0.08 (times 8)
netK = [0.05 + 0.08 * 8 + bend + friction_losses[key] for key, bend in enumerate(bends)]
dPs = []
for key, v in enumerate(velocities):
    dPs.append(f.core.dP_from_K(K=netK[key], rho=WATER_DENSITY, V=v).magnitude * un.Pa)
row1_pressures = [P - dPs[key] for key, P in enumerate(supply_pressure_list)]
#print("starting pressure", supply_pressure_list)
Ps = [[],[]]
for key, P in enumerate(row1_pressures):
    #print("row 1", P)
    #print(key)
    Ps[key].append(P.to('ftH2O'))
row_1_friction_factors = [f.friction.friction_factor(Re=r, eD=pvc_eD) for r in reynolds_numbers]
row_1_friction_loss = [f.core.K_from_f(f=fric, L=12, D=ABS['Di']) for fric in row_1_friction_factors]
per_row_loss = []
for key, v in enumerate(v_avg):
    per_row_loss.append(f.core.dP_from_K(K=row_1_friction_loss[key], rho=WATER_DENSITY, V=v).magnitude * un.Pa)
for i in range(1, 6, 1):
    for key, P in enumerate(row1_pressures):
        #print("row {}".format(i + 1), P - i*per_row_loss[key])
        Ps[key].append((P - i*per_row_loss[key]).to('ftH2O'))
#print(Ps)
L = 20

#for k1, row_P in enumerate(Ps):
#    for k2, P in enumerate(row_P):
#        print('input pressure: {}, row: {}, H/L value {}'.format(k1 + 1, k2 + 1, L/P.magnitude))

row1 = [P.magnitude for P in Ps[0]]
row2 = [P.magnitude for P in Ps[1]]
uc1 = 1 - np.std(row1)/np.mean(row1)
uc2 = 1 - np.std(row2)/np.mean(row2)

arr1 = np.ones((6,10))
arr2 = np.ones((6,10))
arr1[0][0] *= uc1
arr2[0][0] *= uc2

for k, r in enumerate(arr1):
    if k > 0:
        arr1[k][0] = arr1[k-1][0] * uc1
    for k2, c in enumerate(r):
        if k2 > 0:
            r[k2] = r[k2-1] * uc1


for k1, r in enumerate(arr2):
    if k1 > 0:
        arr2[k1][0] = arr2[k1-1][0] * uc2
    for k2, c in enumerate(r):
        if k2 > 0:
            r[k2] = r[k2-1] * uc2

#ax1 = sns.heatmap(arr1)
#ax2 = sns.heatmap(arr2)
#arr1 *= arr1.max() - arr1.min()
#arr2 *= arr2.max() - arr2.min()
#arr2 *= supply_pressure_list[1].magnitude
arr3 = np.concatenate((arr1, arr2), axis=1)
ax = plt.axes()
#sns.heatmap(arr3, ax=ax, xticklabels=['Gravity Supply','\t','\t','\t','\t', 'Municipal Supply'], yticklabels=False, cmap='YlGnBu_r')
sns.heatmap(arr3, ax=ax, xticklabels=False, yticklabels=False, cmap='YlGnBu', annot=True)
ax.set_title('Gravity vs Municipal Water Supply Uniformity', fontname="Liberation Sans", size="40")
plt.show()
#sns.plt.show()

